function [u, u_hist, u_loading, err_hist, conv_svM, R, p] = load_stepping(Edof, Ex, Ey, ndof, delta, u_control, bc, mp, method, Nsteps, u, ep, settings, K, eq)
% function [u, u_hist, u_loading, err_hist, conv_svM, R, p] = load_stepping(Edof, Ex, Ey, delta, u_control, bc, mp, eq)
% load_stepping performs the incremental increasing load application of the
% non-linear FE-problem. For each iteration it calls the <full_solve_step>
% function to solve for the converged displacements under the given load 
% condition before incrementing the load closer and closer to the final
% solution of the problem for the given loading condition.
% If different iteration techniques want to be used, a different 
% <full_solve_step> function should be specified. Both the Newton and
% modified Newton iteration techniques are supported in this repository.
%
% Input
% Edof          [nelm, n]       topology matrix of the problem domain in
%                               terms of the nodal degrees of freedom where
%                               n-1 is the number of dof per element.
% Ex            [nelm, mx]      x-coordinates of the nodes of each element
%                               where mx represents the number of x-dof per 
%                               element.
% Ey            [nelm, my]      y-coordinates of the nodes of each element
%                               where my represents the number of y-dof per
%                               element.
% ndof          [scalar]        number of total degrees of freedom in the
%                               domain.   
% delta         [scalar]        prescribed displacement on known boundaries
%                               in the problem domain.
% u_control     [varies, 2]     essential boundary conditions not equal to
%                               zero, i.e. prescribed displacements where 
%                               the load stepping will be controlled from.
% bc            [varies, 2]     essential boundary conditions equal to zero.
% mp            [1, 3]          material properties of [E, nu, sy0] needed
%                               to solve the non-linear FE problem.
% method        [scalar]        iteration method
% Nsteps        [scalar]        number of load steps.
% u             [ndof, 1]       initial zeros guess for the displacments on
%                               all degrees of freedom.
% ep            [1, 4]          element properties needed for numerical 
%                               integration routines.
% settings                      Structure with the following fields of 
%                               iteration settings
%               .tol            Tolerance in terms of machine percision for 
%                               accepting solution
%               .maxiter        Maximum number of iterations to solve the 
%                               time step
%               .minnorm        Minnorm to compute the scaled tolerance
% K             [ndof, ndof]    a initial elastic stiffness matrix used 
%                               only for certain iteration methods (optional)
% eq            [1, 2]          Body load vector [bx, by] (optional)
%
% Output
% u             [ndof, 1]       displacement on all degrees of freedom
% u_hist        [ndof, 3]       history of displacements on the boundary, 
%                               B1, degrees of freedom
% u_loading     [1, Nsteps]     strain controlled load steps
% err_hist    [Nsteps, maxiter] history of the error for each load step for
%                               all Newton iterations.
% conv_svM      [nelm, 1]       von Mises stress in each element averaged 
%                               over each Guass point for the converged 
%                               iteration
% R             [ndof, 1]       Converged internal forces in ALL degrees of
%                               freedom.
% p         [nelm, 2]           convergence rate data of slope and
%                               intercept point for the logarithmic linear
%                               equation.
% 
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% define simulation settings
u_max = delta;                          % [m]    Maximum known displacement 
u_loading = linspace(0, u_max, Nsteps); % set displacement controlled load increments

%% initialize variables
% determine free dof by reverse of both prescribed and essential bc dof
fdof = (1:ndof)';
fdof([u_control; bc(:, 1)]) = [];

% initialize previous load step displacements as u_old
u_old = u;

% initialize zeros matrix to store reaction force in each load step which
% will be outputted to main script.
R = zeros(Nsteps, 1);
u_hist = zeros(Nsteps, size(u_control, 1));
err_hist = zeros(Nsteps, settings.maxiter);

%% compute FE-solution in each load step
for i = 2:Nsteps
    % update u and u_old from previous load step
    du = u - u_old;
    u_old = u;
    
    % insert controlled displacement
    u(u_control) = u_loading(i);
    u(bc(:, 1)) = bc(:, 2);              % reset boundary conditions
    
    % make guess for displacements in current load step
    u(fdof) = u(fdof) + du(fdof);
    
    % compute the new converged displacements for the current load step
    if method == 1
        %BC = [u_control, zeros(size(u_control, 1))];
        %[u, K] = elaststiffness(ndof, Edof, u, Ex, Ey, ep, mp, BC);
        [u, r, err, conv_svM, count, conv] = solve_step_newton(Edof, Ex, Ey, ndof, mp, ep, settings, fdof, u);
    elseif method == 2
        [u, r, err, conv_svM, count, conv] = solve_step_modnewton(Edof, Ex, Ey, ndof, mp, ep, settings, fdof, u, K);
    else
        error('Unsupported iteration method!');
    end
    
    % save history of converged values
    if conv
        k = length(err);
        u_hist(i, :) = u(u_control);        % history of u on boundary, B1
        err_hist(i, 1:k) = err;             % history of err
        R(i, 1) = sum(r(u_control));        % sum reaction force on B1
        if k >= 3
            fit = polyfit(log(err(1:end-1)), log(err(2:end)), 1);
            p(i, :) = fit;
        else
            p(i, :) = [0 0];
        end
        fprintf("Step %3u, iter %3u, error: ", i, k);
        fprintf("%10.3e\t", err(1:k));
        fprintf("force %3f\t", sum(r(u_control))); 
        fprintf("max sigma vM %3f\t", max(conv_svM)); 
        fprintf("rate %d", p(i, 1)); fprintf("\n");
    else
        error('Load step did not converge within %d iterations', count);
    end
end