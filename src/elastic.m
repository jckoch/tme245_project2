function [sigma, dsde] = elastic(epsilon, mp)
% function [sigma, dsde] = elastic(epsilon, mp)
% Material model for linear elasticity
% Input
% epsilon   [6x1]   Strain components   [exx,eyy,ezz,exy,exz,eyz]'
% mp        [1x2]   Material parameters [E, nu]. mp can be larger, but only 
%                   the 2 first components will be used.
% 
% Output
% sigma     [6x1]   Stress components   [sxx,syy,szz,sxy,sxz,syz]'
% dsde      [6x6]   Tangent stiffness matrix d(sigma)/d(epsilon)
% 
% Written by Knut Andreas Meyer, FEM Structures 2017
% =========================================================================

E = mp(1);
v = mp(2);
dsde = hooke(4, E, v);

sigma = dsde*epsilon;