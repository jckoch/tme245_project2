function [eps, sig, conv] = solve_step(eps, mp, settings)
% function [eps, sig, conv] = solve_step(eps, mp)
% solve_step solves the strain controlled step by calculating sigma (sig)
% given the strain eps. The strain components 11, 12, 13 and 23 must follow
% the given strain eps (locations 1, 4, 5 and 6 in the vector), and the
% stress components 22 and 33 in the stress sig (locations 2, 3) in the
% vector must remain zero. 
% 
% Input
% eps   [6,1]   Strain vector: [eps11, eps22, eps33, eps12, eps13, eps23]
%               Note that eps11, eps12, eps13 and eps23 should not be
%               changed. eps22 and eps33 should be changed.
% mp    [1,3]   Material parameters for hencky [E, nu, sy]
% settings      Structure with the following fields of iteration settings
%   .tol        Tolerance (in stress units) for accepting solution
%   .maxiter    Maximum number of iterations to solve the time step
% 
% Output
% eps   [6,1]   Same as input, but with updated eps22 and eps33.
% sig   [6,1]   Calculated stress for the given strain increment, note that
%               the constraint that sig22 and sig33 should be zero should
%               be fulfilled approximately within the tolerance (i.e. these
%               components should not explicitly be set to zero)
% conv  boolean true if a solution is found for the given load step within
%               the maximum number of iterations, and false otherwise.
% 
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% Read in iteration settings
tol     = settings.tol;
maxiter = settings.maxiter;

%% Assume that it hasn't converged, and set conv=true when it does.
conv = false;

%% Using the Newton iteration technique
% it is now your job to solve the problem of finding which eps22 and eps33 
% that gives zero sig22 and sig33!

count = 0;
while ~conv && count <= maxiter

    % calculate stress due to first strain guess
    [sig, dsde] = hencky(eps, mp);
    
    % error
    err = norm(sig(2:3));
    
    if err < tol
        conv = true;
        sig(2:3) = zeros(2, 1);
        break;
    else
    
        % compute del_eps
        del_eps = -dsde(2:3, 2:3)\sig(2:3);

        % update strain
        eps(2:3) = eps(2:3) + del_eps;
    
    end
    
    % increase counter
    count = count + 1;
end

end




