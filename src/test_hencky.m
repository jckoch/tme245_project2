% test_hencky
% Author: James Koch and Amer Bitar
%--------------------------------------------------------------------------
% PURPOSE
% To test and verify the correctness of the function "hencky" with external
% stress and stiffness data.
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% set inputs
% test strain values
epsilon=[1.3; 0.4; -0.4; 0.3; 0.1; -0.2]*10^(-3);

% test material properties
mp=[210e3, 0.3, 220];

%% run hencky function to verify and validate outputs
[sigma, dsde]=hencky(epsilon, mp);

% epsilon_elast = epsilon*10^(-3);
% [sigma_elast, dsde_elast]=elastic(epsilon_elast, mp);
% [sigma_elast_hencky, dsde_elast_hencky]=hencky(epsilon_elast, mp);

sigma_validation = ([353.911 222.638 105.951 21.879 7.293 -14.586]'); % MPa
dsde_validation = [200004.328, 129158.732, 195836.940, -12502.164, -4167.388,  8334.776;
                   129158.732, 272132.197, 123709.071,    480.852,   160.284,  -320.568;
                   195836.940, 123709.071, 205453.989,  12021.312,  4007.104, -8014.208;
                   -12502.164,    480.852,  12021.312,  70765.454,  -721.279,  1442.557;
                    -4167.388,    160.284,   4007.104,   -721.279, 72688.864,   480.852;
                     8334.776,   -320.568,  -8014.208,   1442.557,   480.852, 71967.585];

tol = 10e-3; % set matlab machine precision
tf_sigma = abs(sigma - sigma_validation) < tol;
tf_dsde = abs(dsde - dsde_validation) < tol;

if all(tf_sigma) == 0
    warning('Stress validation has failed.');
end

if all(tf_dsde) == 0
    warning('Tangent stiffness matrix validation has failed.');
end