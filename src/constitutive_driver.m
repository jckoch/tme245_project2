%% Shell for constitutive driver in CA2 - FEM Structures
% Written by Knut Andreas Meyer, 2018-03-16
clc;
close all;

%% Settings
% Material parameters
E  = 210e3; %[MPa]
nu = 0.3;   %[-]
sy0= 220;   %[MPa]
mp = [E, nu, sy0];

% Simulation settings
eps11_max = 1e-2;   %[-]    Maximum strain
Nsteps = 100;       %[-]    Number of load steps

% Iteration settings
settings.tol = 1e-6;
settings.maxiter = 20;

% Post processing settings
sig_comp_plot = [1, 2, 4];
eps_comp_plot = [1, 2];

%% Main program
eps11_loading = linspace(0, eps11_max, Nsteps);

eps = zeros(6,1);
sig = zeros(6,1);
eps_old = eps;

eps_hist = zeros(length(eps_comp_plot), Nsteps);
sig_hist = zeros(length(sig_comp_plot), Nsteps);

for i = 2:Nsteps
    % Save old and delta strain
    delta_eps = eps-eps_old;
    eps_old = eps;
    
    % Insert the controlled strain
    eps(1) = eps11_loading(i);  % 11 component controlled with ramp
    eps(4:6) = 0;               % Shear components controlled to zero
    
    % Make a good guess for the strain
    eps(2:3) = eps_old(2:3) + delta_eps(2:3);
    
    % Solve the current time step
    [eps, sig, conv] = solve_step(eps, mp, settings);
    
    if conv
        % Save variables for later plot
        eps_hist(:, i) = eps(eps_comp_plot);
        sig_hist(:, i) = sig(sig_comp_plot);
    else
        error('The step did not converge');
    end
end

%% Post processing
numbering = {'11', '22', '33', '12', '13', '23'};
for i = 1:length(sig_comp_plot)
    figure(i);
    plot(eps11_loading, sig_hist(i, :));
    xlabel('\epsilon_{11}');
    ylabel(['\sigma_{', numbering{sig_comp_plot(i)}, '}']);
    if i == 1
        saveas(i, 'imgs/uniaxialstraintest', 'eps');
    end
end

for j = 1:length(eps_comp_plot)
    figure(j+i);
    plot(eps11_loading, eps_hist(j, :));
    xlabel('\epsilon_{11}');
    ylabel(['\epsilon_{', numbering{eps_comp_plot(j)}, '}']);
end