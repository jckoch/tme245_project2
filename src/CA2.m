% Project 2
% Author: James Koch and Amer Bitar
%--------------------------------------------------------------------------
% PURPOSE
% FE-method to solve a non-linear elasticity problem. This problem is 
% solved in 2-dimensions with known thickness in the third direction. 
% 
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

close all;
clear all; clc;

%% define geometric properties regarding the domain
% geometry
Ri = 0.3/2;         %m
Ro = 0.5/2;         %m

%% define FE mesh
% define approximate element sizes
hr = 0.010;         %m
hphi = 0.010;       %m

% generate FE mesh
elemtype = 'quad4'; % element type
[Edof, Ex, Ey, B1, B2, B3, B4, P1, P2, P3, P4] = ringmesh(Ri, Ro, elemtype, hr, hphi);

% compute total number of degrees of freedom
ndof = max(Edof(:));

%% define boundary conditions
% prescribed displacement
delta = -0.006/2;   %m

% boundary degrees of freedom
u_control = B1(:, 2);                   % loading controlled
bc = [B3(:, 2), zeros(size(B3, 1), 1);  % loading uncontrolled
      B3(1, 1), 0];                     % to prevent rigid body motion

%% define material parameters
% material properties
E = 210e9;          %MPa
nu = 0.3;
sy0 = 220e6;        %MPa
mp = [E, nu, sy0];

% material model
mmodel = [1 2 2];

%% define iteration methods
methodlist = [1 1 2];

%% define tolerances
tol = [1e-6 1e-12 1e-7];
    
%% initialize variables
Nsteps = 300;                       % [-]   Number of load steps
Rtot = zeros(Nsteps, 2);            % [N]   Output matrix for opening force on each load step
utot = zeros(ndof, 2);
svMtot = zeros(size(Edof, 1), 2);
ptot = zeros(Nsteps, 2);
mm = 1;                             % [-]   Loop counter
  
%% for loop to run different material models
% iteration 1: linear elasticity
% iteration 2: hencky plasticity
for m = 1:length(mmodel) 
    % initialize variables
    u = zeros(ndof, 1);          % [m]   Displacement vector for ALL dofs
    
    % set iteration method
    method = methodlist(m);      % 1=newton; 2=modified newton
    
    % define element properties
    ptype = 2; % plane strain
    t = 0.5;   % [m], thickness
    ir = 2;    % Integration rule (number of gauss points in each direction)
    ep = [ptype, t, ir, mmodel(m)];
    
    % define body load if needed
    % NOT NEEDED HERE SINCE IT IS EQUAL TO ZERO
    
    % define iteration settings
    settings.tol = tol(m);       % [-]  Machine percision
    settings.maxiter = 20;       % [-]  Maximum iterations in Newton's method
    settings.minnorm = 1e-12;    % [-]  Minnorm to compute the scaled tolerance
    
    % compute constant elastic stiffness matrix, K, if using modified
    % Newton method; 1=newton and 2=modified newton
    if method == 1 
        [u, u_hist, u_loading, err_hist, conv_svM, R, p] = load_stepping(Edof, Ex, Ey, ndof, delta, u_control, bc, mp, method, Nsteps, u, ep, settings);
    elseif method == 2
        % initialize variables
        N = 20;
        K = spalloc(ndof, ndof, N*ndof);

        % compute displacements per element
        ed = extract(Edof, u);

        % Element routines
        for e = 1:size(Edof, 1)
            % define element displacement guess
            ue = ed(e, :)';

            % compute element stiffness and internal/external forces
            [Ke, ~, ~, ~] = elem4n(ue, Ex(e, :), Ey(e, :), ep, mp);

            % assemble into global stiffness matrix
            K(Edof(e,2:end),Edof(e,2:end)) = K(Edof(e,2:end),Edof(e,2:end)) + Ke;
        end      
        [u, u_hist, u_loading, err_hist, conv_svM, R, p] = load_stepping(Edof, Ex, Ey, ndof, delta, u_control, bc, mp, method, Nsteps, u, ep, settings, K);
    else
        error('Unsupported iteration method!');
    end
    
    % extract displacments
    ed = extract(Edof, u);
    
    % save displacements for each material model
    utot(:, m) = u;
    
    % save vonMises stress for each material model
    svMtot(:, m) = conv_svM;
        
    % save reaction force in each load step for given material model
    Rtot(:, m) = R;
    
    % Plot the opening force with respect to controlled displacement
    figure(2);
    hold on;
    plot(abs(u_loading'), abs(R));
    hold off;
    
    % save convergence rate of method
    ptot(:, mm:mm+1) = p;
    
    % Convergence rate plot
    if mmodel(m) == 2
        figure(3);
        hold on;
        if method == 1
            truerate = 2;
        elseif method == 2
            truerate = 1;
        end
        [~, idx] = min(abs(truerate - ptot(:, mm)));
        loglog(log(err_hist(idx, 1:2)), log(err_hist(idx, 2:3)));
        hold off;
    end

    % update loop counter (used in convergence rate plot part)
    mm = mm + 2;
end

% Load comparison data from Abaqus (as csv file)
abaqus_data = dlmread('src/abaqus_data.csv');

% continue with opening force figure (2)
figure(2);
hold on;
plot(linspace(0, -delta, size(abaqus_data, 1)), abs(abaqus_data(:, 2)));
xlabel('Deflection on slit surface [mm]');
ylabel('Required opening force [N]');
legend('Elastic', 'Hencky', 'Abaqus');
title('Reaction force on slit surface as a function of the deflection');
%saveas(2, 'imgs/openingforce', 'eps')
hold off;

% continue with convergence rate plot
figure(3);
hold on;
xlabel('|g_(k)|');
ylabel('|g_(k+1)|');
title('Convergence Rate');
legend('Newton', 'Modified Newton', 'Location', 'northwest');
%saveas(3, 'imgs/convergencerate', 'eps');
hold off;

%% Extract element displacements
ed1 = extract(Edof, utot(:, 1));            % for linear elasticity
ed2 = extract(Edof, utot(:, 2));            % for hencky plasticity

%% Plot the converged problem domain
figure(1);
plotpar = [3, 2, 0];
sfac = 25;
eldisp2(Ex, Ey, ed2, plotpar, sfac);
xlabel('x');
ylabel('y');
title('Meshed problem domain');
%saveas(1, 'imgs/deformedshape', 'eps');

%% Plot von Mises stress
Exd2 = Ex + sfac*ed2(:, 1:2:7);
Eyd2 = Ey + sfac*ed2(:, 2:2:8);

figure(4);
hold on; axis equal;
colorbar;
colormap jet;
fill(Exd2', Eyd2', svMtot(:, 2));
xlabel('x');
ylabel('y');
title('von Mises stress in the problem domain');
%saveas(4, 'imgs/vonmises_matlab', 'eps');