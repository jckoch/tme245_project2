function [ustar, r, err, conv_svM, count, conv] = solve_step_newton(Edof, Ex, Ey, ndof, mp, ep, settings, fdof, ustar, eq)
% function [ustar, g, r, vonMises, conv] = full_solve_step(ustar, Edof, Ex, Ey, ndof, fdof, ep, mp, settings, eq)
% solve_step solves the displacement controlled load stepping problem by
% calculating the out-of-balance vector, g, from the difference in internal
% and external forces for a given displacement, u. This function uses the
% Newton iteration method to reach convergence in each load step. The 
% constrained displacements must remain the same as in the given 
% displacement, u, and % the out-of-balance force vector, g, in the 
% constrained degrees of freedom must remain zero.
%
% Input
% Edof      [nelm, n]   topology matrix of the problem domain in terms of 
%                       the nodal degrees of freedom where n-1 is the 
%                       number of dof per element.
% Ex        [nelm, mx]  x-coordinates of the nodes of each element where mx
%                       represents the number of x-dof per element.
% Ey        [nelm, my]  y-coordinates of the nodes of each element where my
%                       represents the number of y-dof per element.
% ndof      [ndof,1]    Number of degrees of freedom in total system.
% mp        [1,3]       material parameters for hencky [E, nu, sy]
% ep        [1, 4]      element properties needed for numerical integration
%                       routines. [ptype, t, ir, mmodel]
% settings              Structure with the following fields of iteration 
%                       settings
%           .tol        Tolerance in terms of machine percision for 
%                       accepting solution
%           .maxiter    Maximum number of iterations to solve the time step
%           .minnorm    Minnorm to compute the scaled tolerance
% ustar [ndof,1]        displacement vector containing the initial guess 
%                       for the load-step under consideration in ALL nodal 
%                       degrees of freedom. The constrained degrees of 
%                       freedom should NOT be updated.
% fdof      [fdof,1]    Number of FREE degrees of freedom in the system.
% eq        [1,2]       Body load vector [bx, by] (optional)
% 
% Output
% ustar     [ndof,1]    Same as input, but with updated free degrees of 
%                       freedom.
% r         [ndof, 1]   Converged internal forces in ALL degrees of freedom
% conv_svM  [nelm, 1]  von Mises stress in each element averaged over each
%                       Guass point for the converged iteration
% err       [1, count]  Error of the residual function in terms of it's 
%                       norm for each newton iteration    
% count     [scalar]    Count of the number of times the newton iteration 
%                       is run
% conv      [boolean]   true if a solution is found for the given load step 
%                       within the maximum number of iterations, and false 
%                       otherwise.
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

% If 6th input argument present, assign body load
if nargin==10
    eq = eq;
else
    eq = zeros(2, 1);
end

%% Read in iteration settings
tol     = settings.tol;             % machine percision
maxiter = settings.maxiter;         % maximum number of iterations to try
minnorm = settings.minnorm;         % minimum norm in scaling tolerance

%% Assume that it hasn't converged, and set conv=true when it does.
conv = false;

%% Using the Newton iteration technique
count = 0;                  % counter to count number of newton iterations
while ~conv && count <= maxiter        
    % Initialize variables
    N = 20;
    K = spalloc(ndof, ndof, N*ndof);
    fint = zeros(ndof, 1);
    fext = zeros(ndof, 1);
    vonMises = zeros(size(Edof, 1), 1);
    
    % compute displacements per element
    ed = extract(Edof, ustar);
    
    % set tolerance for solution based on magnitude of considered problem
    Q = 0;
    
    % Element routines
    for e = 1:size(Edof, 1)
        % define element displacement guess
        ue = ed(e, :)';
        
        % compute element stiffness and internal/external forces
        [Ke, finte, fexte, stress] = elem4n(ue, Ex(e, :), Ey(e, :), ep, mp);
        
        % assemble into global stiffness matrix and internal/external vectors
        K(Edof(e,2:end),Edof(e,2:end)) = K(Edof(e,2:end),Edof(e,2:end)) + Ke;
        fint(Edof(e,2:end)) = fint(Edof(e,2:end)) + finte;
        fext(Edof(e,2:end)) = fext(Edof(e,2:end)) + fexte;
        
        % compute von Mises stress
        avgstress = mean(stress, 2)';
        v = [1 1 1 2 2 2];
        Ic = diag(v);
        dl = [1 1 1 0 0 0]';
        Icdev = Ic - (1/3)*dl*(dl');
        vonMises(e, 1) = sqrt(3/2)*sqrt(avgstress*Icdev*(avgstress'));
        
        % determine magnitude of the sum of internal forces for scaling of
        % tolerance
        Q = Q + norm(finte);
    end

    % compute out-of-balance vector
    g = fint(fdof) - fext(fdof);
    
    % compute error criterion
    err(count+1) = norm(g);
    TOL = tol * max(Q, minnorm);
    
    % check error criterion
    if err(count+1) < TOL
        conv = true;
        r = fint;
        conv_svM = vonMises;
        break;
    elseif err(count+1) > TOL && count == maxiter
        conv = false;
        r = 0;
        conv_svM = 0;
    end
    
    % compute change in displacement in the free degrees of freedom
    delta_ustar = -K(fdof, fdof)\g;

    % update "guess" for displacement in the free degrees of freedom
    ustar(fdof) = ustar(fdof) + delta_ustar;
        
    % increase counter
    count = count + 1;
end

end