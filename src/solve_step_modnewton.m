function [ustar, r, err, vonMises, count, conv] = solve_step_modnewton(Edof, Ex, Ey, ndof, mp, ep, settings, fdof, ustar, K, eq)
% function [ustar, g, r, vonMises, conv] = full_solve_step(ustar, Edof, Ex, Ey, ndof, fdof, ep, mp, settings, eq)
% solve_step solves the displacement controlled load stepping problem by
% calculating the out-of-balance vector, g, from the difference in internal
% and external forces for a given displacement, u. This function uses the
% Newton iteration method to reach convergence in each load step. The 
% constrained displacements must remain the same as in the given 
% displacement, u, and % the out-of-balance force vector, g, in the 
% constrained degrees of freedom must remain zero.
%
% Input
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

% If 6th input argument present, assign body load
if nargin==11
    eq=eq;
else
    eq=zeros(2,1);
end

%% Read in iteration settings
tol     = settings.tol;     % machine percision
maxiter = settings.maxiter;
minnorm = settings.minnorm;

%% Assume that it hasn't converged, and set conv=true when it does.
conv = false;

%% Using the Modified Newton iteration technique
count = 0; % counter to count number of newton iterations
while ~conv && count <= maxiter
    % Initialize variables
    fint = zeros(ndof, 1);
    fext = zeros(ndof, 1);
    vonMises = zeros(size(Edof, 1), 1);

    % compute displacements per element
    ed = extract(Edof, ustar);

    % set tolerance for solution based on magnitude of considered problem
    Q = 0;

    % Element routines
    for e = 1:size(Edof, 1)
        % define element displacement guess
        ue = ed(e, :)';

        % compute element stiffness and internal/external forces
        [~, finte, fexte, stress] = elem4n(ue, Ex(e, :), Ey(e, :), ep, mp);

        % assemble into global internal/external vectors
        fint(Edof(e,2:end)) = fint(Edof(e,2:end)) + finte;
        fext(Edof(e,2:end)) = fext(Edof(e,2:end)) + fexte;

        % compute von Mises stress
        avgstress = mean(stress, 2)';
        v = [1 1 1 2 2 2];
        Ic = diag(v);
        dl = [1 1 1 0 0 0]';
        Icdev = Ic - (1/3)*dl*(dl');
        vonMises(e, 1) = sqrt(3/2)*sqrt(avgstress*Icdev*(avgstress'));

        % determine magnitude of the sum of internal forces for scaling of
        % tolerance
        Q = Q + norm(finte);
    end
    
    % compute out-of-balance vector
    g = fint(fdof) - fext(fdof);
    
    % compute error criterion
    err(count+1) = norm(g);
    TOL = tol * max(Q, minnorm);
    
    % check error criterion
    if err(count+1) < TOL
        conv = true;
        r = fint;
        break;
    end
    
    % compute change in displacement in the free degrees of freedom
    delta_ustar = -K(fdof, fdof)\g;

    % update "guess" for displacement in the free degrees of freedom
    ustar(fdof) = ustar(fdof) + delta_ustar;
        
    % increase counter
    count = count + 1;
end

end