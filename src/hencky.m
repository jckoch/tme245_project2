function [sigma, dsde] = hencky(epsilon, mp)
% function [sigma, dsde] = elastic(epsilon, mp)
% Material model for linear elasticity
% Input
% epsilon   [6x1]   Strain components   [exx,eyy,ezz,exy,exz,eyz]'
% mp        [1x2]   Material parameters [E, nu, Sy0].
% 
% Output
% sigma     [6x1]   Stress components   [sxx,syy,szz,sxy,sxz,syz]'
% dsde      [6x6]   Tangent stiffness matrix d(sigma)/d(epsilon)
% 
% Written by Knut Andreas Meyer, FEM Structures 2017
% Modified by: Amer Bittar and James Koch on 2019-02-11
% =========================================================================

%% set input values
E = mp(1);
nu = mp(2);
Sy0 = mp(3);
e = epsilon;

%% define shear modulus and volumetric elastic modulus
G = E/(2*(1+nu));
K = E/(3*(1-2*nu));

%% volumetric strain
v = [1 1 1 0.5 0.5 0.5];
Is = diag(v);

%% deviatoric strain
dl = [1; 1; 1; 0; 0; 0];
Isdev = Is - (1/3)*dl*(dl');

%% determine Gstar variable depending on von Mises strain condition
% define von Mises strain criterion
evm = sqrt(2/3)*sqrt((e')*Isdev*e);

% determine shear modulus based on von Mises strain criterion
if evm <= Sy0/(3*G)
    Gstar = G;
    % dGstar = 0;
else
    Gstar = Sy0/(3*evm);
    % dGstar = -Sy0/(3*evm^2);
end

%% compute secant stiffness matrix and stress vector for given iteration
secantD = 2*Gstar*Isdev + K*dl*(dl');
sigma = secantD*epsilon;

%%  compute tangent stiffness matrix
% dsde = 2*Gstar*Isdev + (4/3)*dGstar*(edev*(edev'))/evm + K*dl*(dl)';
edev = Isdev*e;
if evm < Sy0/(3*G)
    dsde=2*G*Isdev + K*dl*(dl');
else
    dsde=(2*Sy0)/(3*evm)*Isdev- edev *((4*Sy0)/(9*(evm)^3))*(edev') + K*dl*(dl'); 
end

end